#!/bin/bash

set +xe

if [ -f serialreader ]; then
	rm serialreader
fi

env GOOS=linux GOARCH=arm go build -v bitbucket.org/aldrinleal/dataagent/serialreader
env GOOS=linux GOARCH=arm go build -v bitbucket.org/aldrinleal/dataagent/eventforwarder

upx serialreader eventforwarder
