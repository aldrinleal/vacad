#!/usr/bin/env python

import fileinput, time, pyrebase

config = {
  "apiKey": "AIzaSyCQFr6a4DBSsrw-7_5yfeJf8NETIn8vGTI",
  "authDomain": "",
  "databaseURL": "https://cowsurfing-35cfc.firebaseio.com",
  "storageBucket": ""
}

firebase = pyrebase.initialize_app(config)

db = firebase.database()

while True:
	line = raw_input().strip()
	if 0 == len(line):
		print "Skip"
		continue
	print "line: %s (%d)" % (line, len(line))
	event = {
		'clientId': 1,
		'zoneId': 1,
		'tagId': int(line),
		'dateTime': int(time.time())
	}
	event['key'] = "%s-%s-%s" % (event['clientId'], event['zoneId'], event['tagId'])
	db.child("ult_ubicacion").child(event['key']).update(event)