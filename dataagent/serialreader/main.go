package main

import (
	"github.com/tarm/serial"
	"log"
	"os"
	"fmt"
	"strings"
)

func main() {
	c := &serial.Config{Name: os.Args[1], Baud: 115200}

	s, err := serial.OpenPort(c)

	if err != nil {
		log.Fatal(err)
	}

	for true {
		buf := make([]byte, 10)
		n, err := s.Read(buf)
		if err != nil {
			log.Fatal(err)
		}
		line := string(buf[0:n])
		fmt.Println(strings.Trim(line, " \r\n"))
	}
}

