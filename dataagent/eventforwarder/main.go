package main

import (
	"os"
	"bufio"
	"strings"
	"time"
	"strconv"
	"fmt"
	"github.com/zabawaba99/firego"
)

type Event struct {
	Key string `json:"key"`
	ClientId int32 `json:"clientId"`
	ZoneId int32 `json:"zoneId"`
	TagId int32 `json:"tagId"`
	DateTime int32 `json:"dateTime"`
}

func main() {
	reader := bufio.NewReader(os.Stdin)

	for true {
		lineBuf, _, err := reader.ReadLine()

		if nil != err {
			panic(err)
		}

		line := string(lineBuf)

		line = strings.TrimSpace(line)

		if line == "" {
			continue
		}

		tagId, err := strconv.Atoi(line)

		if nil != err {
			panic(err)
		}

		ev := &Event {
			"",
			1,
			1,
			int32(tagId),
			int32(time.Now().Unix()),
		}

		ev.Key = fmt.Sprintf("%d-%d-%d", ev.ClientId, ev.ZoneId, ev.TagId)

		fmt.Printf("%+v\n", ev)

		firebase := firego.New(fmt.Sprintf("https://cowsurfing-35cfc.firebaseio.com/ult_ubicacion/%s", ev.Key), nil)

		if err := firebase.Set(ev); err != nil {
			panic(err)
		}

	}
}